from scrapy.selector import Selector
import requests
from random import randrange, choice
from flask import Flask, Response
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

def get_poem():
    fetch_data = [
        [f'hafez/ghazal/sh{randrange(1,495 + 1)}']
    ]
    selected_p = choice(fetch_data)
    html = requests.get(f"https://ganjoor.net/{selected_p}/").content
    selector = Selector(text=html)
    poem_m1 = selector.css("article .b .m1 p ::text").extract()
    poem_m2 = selector.css("article .b .m2 p ::text").extract()
    return list(zip(poem_m1, poem_m2))


@app.route("/", methods=["GET"])
def random_poem():
    data = get_poem()
    return Response(json.dumps(data, ensure_ascii=False).encode('utf8'),mimetype="application/json")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)