const express = require('express')
const app = express()

app.use(express.static('pages'))

app.get("/", (req,res)=>{
    res.sendFile(__dirname+"/pages/index.html")
})

app.listen(8000, "0.0.0.0")